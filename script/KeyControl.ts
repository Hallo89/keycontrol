'use strict';

export interface Options {
  useGamepad: boolean | 'bypass';
}

interface ActiveKeys {
  code: Map<string, string[]>;
  key: Set<string>;
}
interface KeyData {
  modifiers: Array<'ctrlKey' | 'shiftKey' | 'altKey' | 'metaKey'>;
  key: string[];
  code: string[];
}
interface ActionData {
  callbacks: Callback[];
  keys: KeyData[];
}

// ---- Action ----
export type Extend = Action | Extend[];
export type Callback = () => void;
// TODO Add number types when implementing gamepad
export type Keys = Array<string[] | string>;

export interface GamepadOptions {
  interval?: number;
  delay?: number;
}
export interface Action {
  fn: Callback[] | Callback;
  keys: Keys;
  extend?: Extend[];
  // trigger?: 'down' | 'up';
  // gamepad?: GamepadOptions;
}

/** Gamepad buttons mapped to easier to recognize trivial names */
export const GAMEPAD_BUTTONS = [
  'rightPadDown',
  'rightPadRight',
  'rightPadLeft',
  'rightPadUp',
  'LB',
  'RB',
  'LT',
  'RT',
  'frontButtonLeft', // Select/back
  'frontButtonRight', // Start/forward
  'leftStickPressed',
  'rightStickPressed',
  'leftPadUp',
  'leftPadDown',
  'leftPadLeft',
  'leftPadRight',
  'centerButton',
];

export class KeyControlError extends Error {
  // @ts-ignore
  constructor(...args) {
    super(...args);
    this.name = this.constructor.name;
  }
}

export class KeyControl {
  static GAMEPAD_BUTTONS = GAMEPAD_BUTTONS;

  #actionList: Map<string, ActionData> = new Map();
  #keysPressed: ActiveKeys = {
    code: new Map(),
    key: new Set(),
  };

  // gamepadLoopID: number;
  // gamepads: Record<number, Gamepad> = {};

  constructor(target: EventTarget = window, options: Options) {
    // this.gamepadLoop = this.gamepadLoop.bind(this);

    target.addEventListener('keydown', this.#keyDown.bind(this));
    target.addEventListener('keyup', this.#keyUp.bind(this));

    // if (useGamepad) {
    //   window.addEventListener('gamepadconnected', this.gamepadConnected.bind(this));
    //   window.addEventListener('gamepaddisconnected', this.gamepadDisconnected.bind(this));
    // }
  }

  // ---- Gamepad functions ----
  // gamepadConnected(e: GamepadEvent) {
  //   this.gamepads[e.gamepad.index] = e.gamepad;
  //   // TODO ????
  //   this.gamepadLoopID = requestAnimationFrame(this.gamepadLoop);
  // }
  // gamepadDisconnected(e: GamepadEvent) {
  //   cancelAnimationFrame(this.gamepadLoopID);
  //   delete this.gamepads[e.gamepad.index];
  // }

  // gamepadLoop(e: GamepadEvent) {
  //   for (const [ gamepadIndex, gamepad ] of Object.entries(this.gamepads)) {
  //     for (const [ actionName, action ] of Object.entries(this.#actionList.gamepad)) {

  //       const actionButtons = action.keys.filter(val => typeof val == 'number');
  //       for (const button of actionButtons) {
  //         if (gamepad.buttons[button].pressed && (action.gamepadTimeout == null || action.$isReady)) {
  //           action.fn.call(e.currentTarget, e, actionName);

  //           if (action.gamepadTimeout != null) {
  //             action.$isReady = false;
  //             setTimeout(function() {
  //               action.$isReady = true;
  //             }, action.gamepadTimeout);
  //           }
  //         }
  //       }
  //     }
  //   }

  //   requestAnimationFrame(this.gamepadLoop);
  // }


  // ---- key functions ----
  #keyDown(e: KeyboardEvent) {
    if (this.#keysPressed.code.has(e.code)) {
      this.#keysPressed.code.get(e.code)!.push(e.key);
    } else {
      this.#keysPressed.code.set(e.code, [ e.key ]);
    }
    this.#keysPressed.key.add(e.key);

    for (const action of this.#actionList.values()) {
      for (const keys of action.keys) {
        if (keys.modifiers.every(mod => e[mod] === true)
          && keys.code.every(code => this.#keysPressed.code.has(code))
          && keys.key.every(key => this.#keysPressed.key.has(key))
        ) {
          action.callbacks.forEach(callback => callback());
          // Only call once per action
          break;
        }
      }
    }
  }
  #keyUp(e: KeyboardEvent) {
    this.#keysPressed.code.get(e.code)
      ?.forEach(key => this.#keysPressed.key.delete(key));
    this.#keysPressed.code.delete(e.code);
  }


  // ---- API ----
  addActions(actions: Record<string, Action>) {
    for (var actionName in actions) {
      this.addAction(actionName, actions[actionName]);
    }
  }
  addAction(actionName: string, action: Action) {
    if (!action.fn || !action.keys) {
      throw new KeyControlError(`KeyControl @ addAction: The properties "fn" and "keys" must both be present.`);
    }

    if (action.extend) {
      this.#resolveExtend(action.extend, action);
    }
    this.#registerAction(actionName, action.keys, action.fn);
  }

  removeAction(actionName: string) {
    this.#actionList.delete(actionName);
  }


  // ---- Helper functions ----
  #registerAction(actionName: string, keys: Keys, callbacks: Callback[] | Callback) {
    const actionData: ActionData = {
      callbacks: Array.isArray(callbacks) ? callbacks : [callbacks],
      keys: []
    };
    for (let orKeys of keys) {
      const keyData: KeyData = {
        modifiers: [],
        code: [],
        key: []
      };

      if (!Array.isArray(orKeys)) {
        orKeys = [orKeys];
      }
      for (const key of orKeys) {
        const lower = key.toLowerCase();
        if (lower === 'shift' || lower === 'ctrl' || lower === 'alt' || lower === 'meta') {
          keyData.modifiers.push(`${lower}Key`);
        } else if (key.startsWith('{') && key.endsWith('}')) {
          keyData.code.push(key.slice(1, -1));
        } else {
          keyData.key.push(key);
        }
      }

      actionData.keys.push(keyData);
    }

    this.#actionList.set(actionName, actionData);
  }

  #resolveExtend(extend: Extend[], targetAction: Action) {
    for (let i = extend.length; i >= 0; i--) {
      const entry = extend[i];
      if (Array.isArray(entry)) {
        this.#resolveExtend(entry, targetAction);
      } else {
        this.#extendAction(entry, targetAction);
      }
    }
  }

  #extendAction(extend: Action, targetAction: Action) {
    for (const property in extend) {
      // Merge the "keys" property with shallow uniqueness
      if (property === 'keys') {
        for (const keys of extend.keys) {
          if (!targetAction.keys.includes(keys)) {
            targetAction.keys.push(keys);
          }
        }
      } else {
        // @ts-ignore
        targetAction[property as keyof Action] ??= extend[property as keyof Action];
      }
    }
  }
}
