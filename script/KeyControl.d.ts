export interface Options {
    useGamepad: boolean | 'bypass';
}
export type Extend = Action | Extend[];
export type Callback = () => void;
export type Keys = Array<string[] | string>;
export interface GamepadOptions {
    interval?: number;
    delay?: number;
}
export interface Action {
    fn: Callback[] | Callback;
    keys: Keys;
    extend?: Extend[];
}
/** Gamepad buttons mapped to easier to recognize trivial names */
export declare const GAMEPAD_BUTTONS: string[];
export declare class KeyControlError extends Error {
    constructor(...args: any[]);
}
export declare class KeyControl {
    #private;
    static GAMEPAD_BUTTONS: string[];
    constructor(target: EventTarget | undefined, options: Options);
    addActions(actions: Record<string, Action>): void;
    addAction(actionName: string, action: Action): void;
    removeAction(actionName: string): void;
}
